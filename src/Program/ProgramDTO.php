<?php

namespace drew\webcap\Program;

use drew\webcap\Shell\Shell;
use drew\webcap\Shell\Environment;

/**
 * Description
 */
abstract class ProgramDTO
{
    /**
     * @var Environment
     */
    protected $env;

    /**
     * @var int
     */
    protected $xserverNum;

    /**
     * @var Shell
     */
    protected $shell;

    public function __construct()
    {
        $this->env = new Environment();
    }

    public function addEnv(string $name, string $value)
    {
        $this->env->set($name, $value);
    }

    public function getEnv(): Environment
    {
        return $this->env;
    }

    public function setXserverNum(int $number)
    {
        $this->xserverNum = $number;
    }

    public function getXserverNum()
    {
        return $this->xserverNum;
    }

    public function setShell(Shell $shell)
    {
        $this->shell = $shell;
    }

    public function getShell()
    {
        return $this->shell;
    }
}
