<?php

namespace drew\webcap\Program\Shooter;

use drew\webcap\Program\Program;

abstract class Shooter extends Program
{
    /**
     * @var ShooterDTO
     */
    protected $dto;
    /**
     * @var string
     */
    protected $destination = '';

    public function __construct(ShooterDTO $dto)
    {
        $this->dto = $dto;
        $this->dto->addEnv('DISPLAY', ':' . $this->dto->getXserverNum());
    }

    public function setDestination(string $destination)
    {
        $this->destination = $destination;
    }

    public function getDestination(): string
    {
        return $this->destination;
    }
}
