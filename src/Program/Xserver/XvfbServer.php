<?php

namespace drew\webcap\Program\Xserver;

/**
 * Description
 */
class XvfbServer extends Xserver
{
    protected function getCmdLine(): string
    {
        return sprintf('Xvfb :%d -screen 0 %sx%d -nocursor -retro',
                $this->dto->getXserverNum(), $this->dto->getResolution(), $this->dto->getDepth());
    }

    public static function getFree(): int
    {
        foreach (range(10, 12) as $id) {
            $cmd = sprintf("ps ax | grep %s | grep ':%d'", 'Xvfb', $id);
            $output = trim(shell_exec($cmd));
            var_dump($output);
            $lines = explode("\n", $output);
            if (count($lines) == 1) {
                return $id;
            }
        }
        throw new \Exception('No free Xvfb servers left');
    }
}
