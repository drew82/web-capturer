<?php

namespace drew\webcap\Program\Xserver;

use drew\webcap\Program\ProgramDTO;

/**
 * Description
 */
class XserverDTO extends ProgramDTO
{
    protected $resolution;
    protected $depth;

    public function setResolution($resolution)
    {
        $this->resolution = $resolution;
    }

    public function getResolution()
    {
        return $this->resolution;
    }

    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    public function getDepth()
    {
        return $this->depth;
    }
}
