<?php

namespace drew\webcap\Program\Xserver;

use drew\webcap\Program\Program;

/**
 * Description
 */
abstract class Xserver extends Program
{
    /**
     * @var XserverDTO
     */
    protected $dto;

    public function __construct(XserverDTO $dto)
    {
        $this->dto = $dto;
    }

    abstract public static function getFree(): int;
}
