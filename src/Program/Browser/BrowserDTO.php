<?php

namespace drew\webcap\Program\Browser;

use drew\webcap\Program\ProgramDTO;

/**
 * Description
 */
class BrowserDTO extends ProgramDTO
{
    protected $name;
    protected $screenResolution;
    protected $url;
    protected $tmpDir;

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return $this->name;
    }

    function setScreenResolution(string $screenResolution)
    {
        $this->screenResolution = $screenResolution;
    }

    function getScreenResolution(): string
    {
        return $this->screenResolution;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setTmpDir($dir)
    {
        $this->tmpDir = $dir;
    }

    public function getTmpDir()
    {
        return $this->tmpDir;
    }
}
