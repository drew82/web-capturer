<?php

namespace drew\webcap\Program\Browser;

/**
 * Description
 */
class VivaldiBrowser extends Browser
{
    protected function getCmdLine(): string
    {
        return sprintf('vivaldi --user-data-dir=%s --start-maximized %s',
                $this->dto->getTmpDir(), $this->dto->getUrl());
    }
}
