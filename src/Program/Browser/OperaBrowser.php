<?php

namespace drew\webcap\Program\Browser;

/**
 * Description
 */
class OperaBrowser extends Browser
{
    protected function getCmdLine(): string
    {
        return sprintf('opera -pd %s -geometry %s+0+0 -fullscreen -nosession -nomail -activetab %s',
                $this->dto->getTmpDir(), $this->dto->getScreenResolution(), $this->dto->getUrl());
    }
}
