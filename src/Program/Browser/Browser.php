<?php

namespace drew\webcap\Program\Browser;

use drew\webcap\Program\Program;

/**
 * Description
 */
abstract class Browser extends Program
{
    protected $audioOut = true;
    protected $videoOut = true;
    /**
     * @var BrowserDTO
     */
    protected $dto;

    public function __construct(BrowserDTO $dto)
    {
        $this->dto = $dto;
        $this->dto->addEnv('DISPLAY', ':' . $this->dto->getXserverNum());
    }
}
