<?php

namespace drew\webcap\Program\Browser;

class FirefoxBrowser extends Browser
{
    protected function getCmdLine(): string
    {
        return sprintf('firefox %s', $this->dto->getUrl());
    }
}
