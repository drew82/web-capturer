<?php

namespace drew\webcap\Program\Recorder;

use drew\webcap\Program\ProgramDTO;

/**
 * Description
 */
class RecorderDTO extends ProgramDTO
{
    protected $viewport;
    protected $offset;
    protected $fps;

    public function setViewport($viewport)
    {
        $this->viewport = $viewport;
    }

    public function getViewport()
    {
        return $this->viewport;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setFps($fps)
    {
        $this->fps = $fps;
    }

    public function getFps()
    {
        return $this->fps;
    }
}
