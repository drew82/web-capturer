<?php

namespace drew\webcap\Program\Recorder;

/**
 * Description
 */
class FfmpegRecorder extends Recorder
{
    protected function getCmdLine(): string
    {
        return sprintf('ffmpeg '
            . '-video_size %s -framerate %d -draw_mouse 0 '
            . '-f x11grab -i :%d.0+%s '
            . '-f pulse -ac 2 -i default '
            . '-vcodec libx264 -pix_fmt yuv420p -preset ultrafast '
            . '-acodec aac -strict -2 '
            . '-y %s',
            $this->dto->getViewport(),
            $this->dto->getFps(),
            $this->dto->getXserverNum(),
            $this->dto->getOffset(),
            $this->getDestination()
        );
    }
}
