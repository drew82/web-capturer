<?php

namespace drew\webcap\Program\Recorder;

use drew\webcap\Program\Program;

/**
 * Description
 */
abstract class Recorder extends Program
{
    protected $audioIn = true;
    protected $videoIn = true;

    /**
     * @var string
     */
    protected $destination = '';

    /**
     * @var RecorderDTO
     */
    protected $dto;

    public function __construct(RecorderDTO $dto)
    {
        $this->dto = $dto;
    }

    public function setDestination(string $destination)
    {
        $this->destination = $destination;
    }

    public function getDestination(): string
    {
        return $this->destination;
    }
}
