<?php

namespace drew\webcap\Program;

use drew\webcap\Program\ProgramDTO;

/**
 * Description
 */
abstract class Program
{
    /**
     * @var ProgramDTO
     */
    protected $dto;
    protected $videoIn  = false;
    protected $videoOut = false;
    protected $audioIn  = false;
    protected $audioOut = false;

    abstract protected function getCmdLine(): string;

    protected function getCmdName()
    {
        preg_match('%^\s*(\w+)(?:\s|$)%', $this->getCmdLine(), $matches);
        return $matches[1];
    }

    public function start()
    {
        $this->dto->getShell()->start($this->getCmdLine(), $this->dto->getEnv());
    }

    public function stop($type = 'TERM')
    {
        $this->dto->getShell()->stop($this->getCmdName(), $type);
    }

    public function exec(): string
    {
        return $this->dto->getShell()->exec($this->getCmdLine(), $this->dto->getEnv());
    }

    public function isRunning(): bool
    {
        return $this->dto->getShell()->isRunning($this->getCmdName());
    }

    public function assertRunning(): bool
    {
        if (!$this->isRunning()) {
            $classname = (new \ReflectionClass($this))->getShortName();
            throw new \Exception($this . ' is not running');
        }
        return true;
    }

    public function assertNotRunning(): bool
    {
        if ($this->isRunning()) {
            throw new \Exception($this . ' is running');
        }
        return true;
    }

    public function __toString()
    {
        return (new \ReflectionClass($this))->getShortName();
    }
}
