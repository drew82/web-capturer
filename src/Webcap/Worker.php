<?php

namespace drew\webcap\Webcap;

use drew\webcap\Shell\Shell;

/**
 * Description
 */
class Worker
{
    public static $homeDir = '/home/';
    public static $startDelay = 0.3;
    public static $stopDelay = 0.3;

    /**
     * @var Shell
     */
    protected $shell;

    /**
     * @var int
     */
    protected $xserverNum;

    /**
     * @var WorkerDTO
     */
    protected $dto;

    public function __construct(Shell $shell, int $xserverNum)
    {
        $this->shell = $shell;
        $this->xserverNum = $xserverNum;
    }

    public function init(Config $config)
    {
        $this->dto = WorkerDTOBuilder::setup($config, $this->shell, $this->xserverNum, static::$homeDir . '/' . $this->shell->getUser());
    }

    public function getConfig(): Config
    {
        return $this->dto->getConfig();
    }

    public function getConfigHash(): string
    {
        return isset($this->dto) ? $this->dto->getConfig()->getHash() : '';
    }

    public function getId(): int
    {
        return $this->xserverNum;
    }

    public static function getIds(): array
    {
        return range(10, 12);
    }

    public function isRunning(): bool
    {
        return isset($this->dto) && $this->dto->getXserver()->isRunning();
    }

    public function start(string $destination)
    {
        // should not be running
        $this->assertStopped();
        // start
        $this->dto->getXserver()->start();
        $this->dto->getRecorder()->setDestination($destination);
        $this->dto->getRecorder()->start();
        $this->dto->getBrowser()->start();
        // wait for recorder
        usleep(static::$startDelay * 1000 * 1000);
        // ensure all are running, if not - stop all and throw exception
        try {
            $this->assertRunning();
        } catch (\Exception $e) {
            try {
                $this->stop();
            } catch (\Exception $e2) {
                // no action
            }
            throw new \Exception($e->getMessage());
        }
    }

    public function stop()
    {
        $this->dto->getBrowser()->stop();
        $this->dto->getRecorder()->stop();
        $this->dto->getXserver()->stop();
        //
        usleep(static::$stopDelay * 1000 * 1000);
        // ensure all are stopped
        $this->assertStopped();
    }

    /**
     * @throws \Exception
     */
    protected function assertRunning()
    {
        $this->dto->getXserver()->assertRunning();
        $this->dto->getRecorder()->assertRunning();
        $this->dto->getBrowser()->assertRunning();
    }

    /**
     * @throws \Exception
     */
    protected function assertStopped()
    {
        $this->dto->getXserver()->assertNotRunning();
        $this->dto->getRecorder()->assertNotRunning();
        $this->dto->getBrowser()->assertNotRunning();
    }

    public function shot(string $destination)
    {
        $this->dto->getShooter()->setDestination($destination);
        return $this->dto->getBrowser()->isRunning() ? $this->shotHot() : $this->shotCold();
    }

    protected function shotHot()
    {
        $this->takeShot();
        return true;
    }

    protected function shotCold()
    {
        $this->startXserver();
        $this->startBrowser();
        sleep(5);
        $this->takeShot();
        $this->dto->getBrowser()->stop();
        $this->dto->getXserver()->stop();
        return true;
    }

    protected function takeShot()
    {
        if ($output = $this->dto->getShooter()->exec()) {
            throw new \Exception(sprintf('Error executing shooter: "%s"', $output));
        }
    }
}
