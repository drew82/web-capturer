<?php

namespace drew\webcap\Webcap;

use MatthiasMullie\Scrapbook\KeyValueStore;
use drew\webcap\Shell\Shell;

/**
 * Description
 */
class WorkerPool extends \SplObjectStorage
{
    /**
     * @var KeyValueStore
     */
    protected $store;

    public function __construct(KeyValueStore $store)
    {
        $this->store = $store;
        $this->init();
    }

    protected function init()
    {
        foreach (Worker::getIds() as $id) {
            $shell  = new Shell($id);
            $worker = new Worker($shell, $id);
            if ($config = $this->store->get($id)) {
                $worker->init($config);
            }
            $this->attach($worker);
        }
    }

    /**
     * @return Worker
     */
    protected function findFree()
    {
        /* @var $worker Worker */
        foreach ($this as $worker) {
            if (!$worker->isRunning()) {
                return $worker;
            }
        }
        throw new \Exception('No free workers left');
    }

    /**
     * @return Worker
     */
    protected function findByConfig(Config $config)
    {
        /* @var $worker Worker */
        foreach ($this as $worker) {
            if ($worker->getConfigHash() == $config->getHash()) {
                return $worker;
            }
        }
        throw new \Exception('Worker is not initiaized');
    }

    /**
     * @return Worker
     */
    public function get(Config $config)
    {
        try {
            return $this->findByConfig($config);
        } catch (\Exception $e) {
            $worker = $this->findFree();
            $worker->init($config);
            return $worker;
        }
    }

    public function __destruct()
    {
        /* @var $worker Worker */
        foreach ($this as $worker) {
            if ($worker->isRunning()) {
                $this->store->set($worker->getId(), $worker->getConfig());
            } else {
                $this->store->delete($worker->getId());
            }
        }
    }
}
