<?php

namespace drew\webcap\Webcap;

use drew\webcap\Shell\Shell;

use drew\webcap\Program\Xserver\Xserver;
use drew\webcap\Program\Xserver\XserverDTO;
use drew\webcap\Program\Xserver\XvfbServer;

use drew\webcap\Program\Browser\Browser;
use drew\webcap\Program\Browser\BrowserDTO;
use drew\webcap\Program\Browser\OperaBrowser;
use drew\webcap\Program\Browser\VivaldiBrowser;
use drew\webcap\Program\Browser\FirefoxBrowser;

use drew\webcap\Program\Recorder\Recorder;
use drew\webcap\Program\Recorder\RecorderDTO;
use drew\webcap\Program\Recorder\FfmpegRecorder;

use drew\webcap\Program\Shooter\Shooter;
use drew\webcap\Program\Shooter\ShooterDTO;
use drew\webcap\Program\Shooter\Scrot;

use drew\webcap\Webcap\WorkerDTO;
use drew\webcap\Webcap\Worker;

/**
 * Description
 */
class WorkerDTOBuilder
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Shell
     */
    protected $shell;

    /**
     * @var int
     */
    protected $xserverNum;

    protected function __construct(Config $config, Shell $shell, int $xserverNum)
    {
        $this->config = $config;
        $this->shell  = $shell;
        $this->xserverNum = $xserverNum;
    }

    public static function setup(Config $config, Shell $shell, int $xserverNum, string $browserTmpDir): WorkerDTO
    {
        $wb = new static($config, $shell, $xserverNum);
        //
        $xserver  = $wb->buildXserver();
        $browser  = $wb->buildBrowser($browserTmpDir);
        $recorder = $wb->buildRecorder();
        $shooter  = $wb->buildShooter();
        //
        $workerDTO = new WorkerDTO();
        $workerDTO->setConfig($config);
        $workerDTO->setXserver($xserver);
        $workerDTO->setBrowser($browser);
        $workerDTO->setRecorder($recorder);
        $workerDTO->setShooter($shooter);
        //
        return $workerDTO;
    }

    protected function buildXserver(): Xserver
    {
        $xserverDTO = new XserverDTO();
        $xserverDTO->setShell($this->shell);
        $xserverDTO->setXserverNum($this->xserverNum);
        $xserverDTO->setResolution($this->config->get(Config::SCREEN_RESOLUTION));
        $xserverDTO->setDepth($this->config->get(Config::SCREEN_DEPTH));
        //
        return new XvfbServer($xserverDTO);
    }

    protected function buildBrowser(string $tmpDir): Browser
    {
        $browserDTO = new BrowserDTO();
        $browserDTO->setShell($this->shell);
        $browserDTO->setXserverNum($this->xserverNum);
        $browserDTO->setName($this->config->get(Config::BROWSER_NAME));
        $browserDTO->setScreenResolution($this->config->get(Config::SCREEN_RESOLUTION));
        $browserDTO->setUrl($this->config->get(Config::BROWSER_URL));
        $browserDTO->setTmpDir(sprintf('%s/%s', $tmpDir, $browserDTO->getName()));
        //
        $class = 'drew\\webcap\\Program\\Browser\\' . ucfirst($browserDTO->getName()) . 'Browser';
        return new $class($browserDTO);
    }

    protected function buildRecorder(): Recorder
    {
        $recorderDTO = new RecorderDTO();
        $recorderDTO->setShell($this->shell);
        $recorderDTO->setXserverNum($this->xserverNum);
        $recorderDTO->setViewport($this->config->get(Config::RECORDER_VIEWPORT));
        $recorderDTO->setOffset($this->config->get(Config::RECORDER_OFFSET));
        $recorderDTO->setFps($this->config->get(Config::RECORDER_FPS));
        //
        return new FfmpegRecorder($recorderDTO);
    }

    protected function buildShooter(): Shooter
    {
        $shoterDTO = new ShooterDTO();
        $shoterDTO->setShell($this->shell);
        $shoterDTO->setXserverNum($this->xserverNum);
        //
        return new Scrot($shoterDTO);
    }
}
