<?php

namespace drew\webcap\Webcap;

/**
 * Description
 */
class Config
{
    const SCREEN_RESOLUTION     = 'screenResolution';
    const SCREEN_DEPTH          = 'screenDepth';
    const BROWSER_NAME          = 'browserName';
    const BROWSER_URL           = 'browserUrl';
    const RECORDER_VIEWPORT     = 'recorderViewport';
    const RECORDER_OFFSET       = 'recorderOffset';
    const RECORDER_FPS          = 'recorderFps';

    protected $data = [
        self::SCREEN_RESOLUTION     => '800x600',
        self::SCREEN_DEPTH          => '16',
        self::BROWSER_NAME          => 'opera',
        self::BROWSER_URL           => 'http://yandex.ru',
        self::RECORDER_VIEWPORT     => '800x600',
        self::RECORDER_OFFSET       => '0,0',
        self::RECORDER_FPS          => 5,
    ];

    public function __construct(array $params)
    {
        foreach ($params as $key => $value) {
            $this->set($key, $value);
        }
    }

    public function set(string $key, $value)
    {
        if (!isset($this->data[$key])) {
            throw new \InvalidArgumentException("Invalid config key '$key'");
        }
        $this->data[$key] = $value;
    }

    public function get(string $key)
    {
        return $this->data[$key];
    }

    public function getHash()
    {
        return md5(serialize($this->data));
    }

    public function getUrl(): string
    {
        return $this->get(self::BROWSER_URL);
    }
}
