<?php

namespace drew\webcap\Webcap;

use drew\webcap\Program\Xserver\Xserver;
use drew\webcap\Program\Browser\Browser;
use drew\webcap\Program\Recorder\Recorder;
use drew\webcap\Program\Shooter\Shooter;

/**
 * Description
 */
class WorkerDTO
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Xserver
     */
    protected $xserver;

    /**
     * @var Browser
     */
    protected $browser;

    /**
     * @var Recorder
     */
    protected $recorder;

    /**
     * @var Shooter
     */
    protected $shooter;

    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }

    public function setXserver(Xserver $xserver)
    {
        $this->xserver = $xserver;
    }

    public function getXserver(): Xserver
    {
        return $this->xserver;
    }

    public function setBrowser(Browser $browser)
    {
        $this->browser = $browser;
    }

    public function getBrowser(): Browser
    {
        return $this->browser;
    }

    public function setRecorder(Recorder $recorder)
    {
        $this->recorder = $recorder;
    }

    public function getRecorder(): Recorder
    {
        return $this->recorder;
    }

    public function setShooter(Shooter $shooter)
    {
        $this->shooter = $shooter;
    }

    public function getShooter(): Shooter
    {
        return $this->shooter;
    }
}
