<?php

namespace drew\webcap\Shell;

/**
 * Description
 */
class Environment
{
    protected $items = [];

    public function set(string $name, string $value)
    {
        $this->items[strtoupper($name)] = strtoupper($value);
    }

    public function __toString()
    {
        $ret = [];
        foreach ($this->items as $name => $value) {
            $ret[] = $name . '=' . $value;
        }
        return implode(' ', $ret);
    }
}
