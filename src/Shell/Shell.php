<?php

namespace drew\webcap\Shell;

use drew\webcap\Shell\Environment;

/**
 * Description
 */
class Shell
{
    public static $username = 'webrec';
    public static $debug = false;

    protected $user;

    public function __construct(int $id)
    {
        $this->user = static::$username . $id;
    }

    public function start(string $cmdLine, Environment $env)
    {
        $cmd = trim(sprintf('%s nice -n 5 sudo -u %s %s >/dev/null 2>&1 & echo $!', $env, $this->user, $cmdLine));
        $output = $this->run($cmd);
        if ($output) {
            return (int)$output;
        } else {
            throw new \Exception(sprintf('Cannot start "%s"', $cmd));
        }
    }

    public function stop(string $cmdName, $type = 'TERM')
    {
		$cmd = sprintf('sudo -u %s pkill -u %s %s', $this->user, $this->user, $cmdName);
        $output = $this->run($cmd);
        if ($output) {
            throw new \Exception(sprintf('Cannot stop "%s": %s', $cmdName, $output));
        }
    }

    public function exec(string $cmdLine, Environment $env): string
    {
        $cmd = trim(sprintf('%s sudo -u %s %s 2>&1', $env, $this->user, $cmdLine));
        $output = $this->run($cmd);
        return $output;
    }

    public function getPid(string $cmdName)
    {
		return $this->run(sprintf('pgrep -u %s %s', $this->user, $cmdName));
    }

    public function isRunning(string $cmdName): bool
    {
		return !empty($this->getPid($cmdName));
    }

    protected function run(string $cmd): string
    {
        if (static::$debug) {
            printf("%s\n", $cmd);
        }
        return trim(shell_exec($cmd));
    }

    public function getUser()
    {
        return $this->user;
    }
}
