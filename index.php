<?php

require_once __DIR__ . '/vendor/autoload.php';

use drew\webcap\Webcap\Config;
use drew\webcap\Webcap\WorkerPool;

$adapter = new League\Flysystem\Adapter\Local(sys_get_temp_dir());
$filesystem = new League\Flysystem\Filesystem($adapter);
$store = new MatthiasMullie\Scrapbook\Adapters\Flysystem($filesystem);

$config = new Config([
    Config::BROWSER_NAME => 'opera',
    Config::BROWSER_URL => 'http://yandex.ru',
]);

$recFile = sprintf('%s/%s-%d.mkv', sys_get_temp_dir(), 'rec', date('U'));
$imgFile = sprintf('%s/%s-%d.png', sys_get_temp_dir(), 'img', date('U'));

$pool = new WorkerPool($store);

$worker = $pool->get($config);

//$worker->start($recFile);
//sleep(15);

//$worker->shot($imgFile);

//$worker->stop();
